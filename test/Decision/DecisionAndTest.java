/*
 * The MIT License
 *
 * Copyright 2019 starkmrk.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package Decision;

import java.util.Map;
import java.util.HashMap;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author starkmrk
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DecisionAndTest {

    private final Map<String, Double> values;
    private DecisionAnd instance;

    public DecisionAndTest() {
        this.values = new HashMap<>();
    }

    @Before
    public void setUp() {
        this.values.put("xy#0", 0.0);
        this.values.put("xz#0", 0.0);
        IDecision c1 = new DecisionReadVar(this.values, "xy#0");
        IDecision c2 = new DecisionReadVar(this.values, "xz#0");
        instance = new DecisionAnd(c1, c2);
    }

    /**
     * Test of Init method, of class DecisionAnd.
     */
    @Test
    public void aTestInitDecision() {
        System.out.println("InitDecision - And");
        boolean result = instance.Init();
        assertTrue(result);
    }

    /**
     * Test of Do method, of class DecisionAnd.
     */
    @Test
    public void bTestDoDecision() {
        System.out.println("DoDecision - And");
        boolean expResult = true;
        instance.Init();
        boolean result = false;
        if (!instance.Do()) {
            this.values.put("xy#0", 1.0);
            if (!instance.Do()) {
                this.values.put("xy#0", 0.0);
                this.values.put("xz#0", 1.0);
                if (!instance.Do()) {
                    this.values.put("xy#0", 1.0);
                    result = instance.Do();
                }
            }
        }
        assertTrue(result);
    }

    /**
     * Test of GetVaribaleName method, of class DecisionAnd.
     */
    @Test
    public void cTestGetVaribaleName() {
        System.out.println("GetVaribaleName - And");
        String expResult = "false (xy#0) && false (xz#0)";
        instance.Init();
        instance.Do();
        String result = instance.GetLog();
        assertEquals(expResult, result);
    }

}
