/*
 * The MIT License
 *
 * Copyright 2019 starkmrk.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package Decision;

import Calculation.CalculationFactory;
import Calculation.ICalculationFactory;
import Extended.NotificationMap;
import java.util.HashMap;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author starkmrk
 */
public class DecisionFactoryTest {

    private final NotificationMap values;

    public DecisionFactoryTest() {
        Map<String, Double> valuesInside = new HashMap<>();
        this.values = new NotificationMap(valuesInside);
    }

    @Before
    public void setUp() {
        this.values.put("xy#0", 0.0);
    }

    /**
     * Test of Init method, of class DecisionFactory.
     */
    @Test
    public void testInit() {
        System.out.println("Init - DecisionFactory");
        try {
            ICalculationFactory calcFactory = new CalculationFactory(this.values);
            DecisionFactory instance = new DecisionFactory(this.values, calcFactory);
            boolean result = instance.Init();
            assertNotEquals(null, result);
        } catch (Exception e) {
            fail("Exception while Init");
        }
    }

    /**
     * Test of GetDecision method, of class DecisionFactory.
     */
    @Test
    public void testGetDecision() {
        System.out.println("GetDecision - DecisionFactory");
        try {
            ICalculationFactory calcFactory = new CalculationFactory(this.values);
            DecisionFactory instance = new DecisionFactory(this.values, calcFactory);
            instance.Init();
            String formular = "xy#0 = true && (false || 5*1 >=6 && true) || FALSE";
            IDecision result = instance.GetDecision(formular, "");
            assertNotEquals(null, result);
        } catch (Exception e) {
            fail("Exception while GetDecision");
        }
    }

}
