/*
 * The MIT License
 *
 * Copyright 2019 starkmrk.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package Decision;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author starkmrk
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DecisionReadConstantTest {

    private DecisionReadConstant instance;

    public DecisionReadConstantTest() {
    }

    @Before
    public void setUp() {
        instance = new DecisionReadConstant("False");
    }

    /**
     * Test of Init method, of class DecisionReadConstant.
     */
    @Test
    public void aTestInitDecision() {
        System.out.println("InitDecision - ReadConstant");
        boolean result = instance.Init();
        assertTrue(result);
    }

    /**
     * Test of Do method, of class DecisionReadConstant.
     */
    @Test
    public void testDoDecision() {
        System.out.println("DoDecision - ReadConstant");
        boolean expResult = true;
        boolean result = false;
        if (!instance.Do()) {
            this.instance = new DecisionReadConstant("True");
            if (instance.Init()) {
                result = instance.Do();
            }
        }
        assertTrue(result);
    }

    /**
     * Test of GetVaribaleName method, of class DecisionReadConstant.
     */
    @Test
    public void testGetVaribaleName() {
        System.out.println("GetVaribaleName - ReadConstant");
        instance.Init();
        String expResult = "false";
        instance.Do();
        String result = instance.GetLog();
        assertEquals(expResult, result);
    }

}
