/*
 * The MIT License
 *
 * Copyright 2019 starkmrk.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package Decision;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author starkmrk
 */
public class IDecisionFactoryTest {

    public IDecisionFactoryTest() {
    }

    @Before
    public void setUp() {
    }

    /**
     * Test of GetDecision method, of class IDecisionFactory.
     */
    @Test
    public void testGetDecision() {
        System.out.println("GetDecision - IDecisionFactory");
        String formular = "";
        IDecisionFactory instance = new IDecisionFactoryImpl();
        IDecision expResult = null;
        IDecision result = instance.GetDecision(formular, "");
        assertEquals(expResult, result);
    }

    /**
     * Test of IsDecision method, of class IDecisionFactory.
     */
    @Test
    public void testIsDecision() {
        System.out.println("IsDecision - IDecisionFactory");
        String formular = "";
        IDecisionFactory instance = new IDecisionFactoryImpl();
        boolean expResult = false;
        boolean result = instance.IsDecision(formular);
        assertEquals(expResult, result);
    }

    public class IDecisionFactoryImpl implements IDecisionFactory {

        public IDecision GetDecision(String formular, String parsedFormular) {
            return null;
        }

        public boolean IsDecision(String formular) {
            return false;
        }
    }

}
