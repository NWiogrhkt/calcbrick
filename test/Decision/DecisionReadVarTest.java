/*
 * The MIT License
 *
 * Copyright 2019 starkmrk.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package Decision;

import java.util.Map;
import java.util.HashMap;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import java.util.regex.Pattern;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author starkmrk
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DecisionReadVarTest {

    private final Map<String, Double> values;
    private DecisionReadVar instance;

    public DecisionReadVarTest() {
        this.values = new HashMap<>();
    }

    @Before
    public void setUp() {
        this.values.put("xz#0", 0.0);
        instance = new DecisionReadVar(this.values,"xz#0");
    }

    /**
     * Test of Init method, of class DecisionReadVar.
     */
    @Test
    public void aaTestRegex() {
        System.out.println("InitDecision - regex");
        Pattern p = Pattern.compile(DecisionReadVar.RegExPattern);
        boolean result = !p.matcher("True").lookingAt();
        result = result && !p.matcher("False").lookingAt();
        result =  result && p.matcher("xy#0").lookingAt();
        assertTrue(result);
    }

    /**
     * Test of Init method, of class DecisionReadVar.
     */
    @Test
    public void aTestInitDecision() {
        System.out.println("InitDecision - ReadVar");
        boolean result = instance.Init();
        assertTrue(result);
    }

    /**
     * Test of Do method, of class DecisionReadVar.
     */
    @Test
    public void testDoDecision() {
        System.out.println("DoDecision - ReadVar");
        boolean expResult = true;
        boolean result = false;
        if (!instance.Do()) {
            this.values.put("xz#0", 1.0);
            result = instance.Do();
        }
        assertTrue(result);
    }

    /**
     * Test of GetVaribaleName method, of class DecisionReadVar.
     */
    @Test
    public void testGetVaribaleName() {
        System.out.println("GetVaribaleName - ReadVar");
        String expResult = "false (xz#0)";
        instance.Do();
        String result = instance.GetLog();
        assertEquals(expResult, result);
    }

}
