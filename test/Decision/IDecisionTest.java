/*
 * The MIT License
 *
 * Copyright 2019 starkmrk.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package Decision;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author starkmrk
 */
public class IDecisionTest {

    public IDecisionTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @Before
    public void setUp() {
    }

    /**
     * Test of Do method, of class IDecision.
     */
    @Test
    public void testDo() {
        System.out.println("Do - IDecision");
        IDecision instance = new IDecisionImpl();
        boolean expResult = false;
        boolean result = instance.Do();
        assertEquals(expResult, result);
    }

    /**
     * Test of Init method, of class IDecision.
     */
    @Test
    public void testInit() {
        System.out.println("InitDecision - IDecision");
        IDecision instance = new IDecisionImpl();
        boolean expResult = false;
        boolean result = instance.Init();
        assertEquals(expResult, result);
    }

    /**
     * Test of GetVaribaleName method, of class IDecision.
     */
    @Test
    public void testGetVaribaleNames() {
        System.out.println("GetVaribaleName");
        IDecision instance = new IDecisionImpl();
        String expResult = "";
        String result = instance.GetLog();
        assertEquals(expResult, result);
    }

    /**
     * Test of GetDecisionStatus method, of class IDecision.
     */
    @Test
    public void testGetDecisionStatus() {
        System.out.println("GetDecisionStatus");
        IDecision instance = new IDecisionImpl();
        boolean expResult = false;
        boolean result = instance.GetDecisionStatus();
        assertEquals(expResult, result);
    }

    public class IDecisionImpl implements IDecision {

        @Override
        public boolean Do() {
            return false;
        }

        @Override
        public boolean GetDecisionStatus() {
            return false;
        }

        @Override
        public boolean Init() {
            return false;
        }

        @Override
        public String GetLog() {
            return "";
        }
    }
}
