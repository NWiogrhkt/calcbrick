/*
 * The MIT License
 *
 * Copyright 2019 starkmrk.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package ControlFunction;

import Decision.DecisionFactory;
import Calculation.CalculationFactory;
import Extended.NotificationMap;
import java.util.HashMap;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author starkmrk
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ControlFunctionLineTest {

    private final NotificationMap values;
    private final CalculationFactory calcFactory;
    private final DecisionFactory deciFactory;

    public ControlFunctionLineTest() {
        Map<String, Double> valuesInside = new HashMap<>();
        this.values = new NotificationMap(valuesInside);
        this.calcFactory = new CalculationFactory(this.values);
        this.deciFactory = new DecisionFactory(this.values, this.calcFactory);
    }

    @Before
    public void setUp() {
        this.values.put("xy#0", 1.0);
        this.values.put("xz#0", 2.0);
        this.calcFactory.Init();
        this.deciFactory.Init();
    }

    /**
     * Test of Init method, of class ControlFunctionLine.
     */
    @Test
    public void aTestInit() {
        System.out.println("Init - ControlFunctionLine");
        ControlFunctionLine instance = new ControlFunctionLine(deciFactory.GetDecision("xz#0=xz#0&&xy#0", ""), null);
        boolean result = instance.Init();
        instance = new ControlFunctionLine(null, calcFactory.GetCalculation("xz#0=xz#0+xy#0", ""));
        result = result && instance.Init();
        assertTrue(result);
    }

    /**
     * Test of Do method, of class ControlFunctionLine.
     */
    @Test
    public void bTestDo() {
        System.out.println("Do - ControlFunctionLine");
        boolean result = false;
        ControlFunctionLine instance = null;
        try {
            instance = new ControlFunctionLine(deciFactory.GetDecision("xz#0=xz#0&&xy#0", ""), null);
            result = instance.Init();
            try {
                instance.Do();
            } catch (Exception e) {
                fail();
            }
        } catch (Exception e) {
            fail("Exception while init ControlFunctionLine.");
        }
        try {
            instance = new ControlFunctionLine(null, calcFactory.GetCalculation("xz#0=xz#0+xy#0", ""));
            result = result && instance.Init();
            try {
                instance.Do();
            } catch (Exception e) {
                fail();
            }
        } catch (Exception e) {
            fail("Exception while init ControlFunctionLine.");
        }
        assertTrue(result);

    }

}
