/*
 * The MIT License
 *
 * Copyright 2019 starkmrk.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package ControlFunction;

import Decision.DecisionFactory;
import Calculation.CalculationFactory;
import Decision.IDecision;
import Extended.NotificationMap;
import java.util.HashMap;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author starkmrk
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ControlFunctionIfTest {

    private final ControlFunctionFactory controlFunctionFactory;
    private IControlFunction controlFunction;
    private final NotificationMap values;
    private final CalculationFactory calcFactory;
    private final DecisionFactory deciFactory;

    public ControlFunctionIfTest() {
        Map<String, Double> valuesInside = new HashMap<>();
        this.values = new NotificationMap(valuesInside);
        this.calcFactory = new CalculationFactory(this.values);
        this.deciFactory = new DecisionFactory(this.values, this.calcFactory);
        this.controlFunctionFactory = new ControlFunctionFactory(this.values, this.calcFactory, this.deciFactory);
    }

    @Before
    public void setUp() {
        this.values.put("xy#0", 1.0);
        this.values.put("xz#0", 2.0);
        this.calcFactory.Init();
        this.deciFactory.Init();
        this.controlFunctionFactory.Init();
        String calculation = "xz#0 = xz#0 + 1;";
        this.controlFunction = this.controlFunctionFactory.GetControlFunction(calculation, "");
    }

    /**
     * Test of Init method, of class ControlFunctionIf.
     */
    @Test
    public void aTestInit() {
        System.out.println("Init - ControlFunctionIf");
        IDecision decision = this.deciFactory.GetDecision("xy#0", "");
        ControlFunctionIf instance = new ControlFunctionIf("", decision, this.controlFunction);
        boolean result = instance.Init();
        assertTrue(result);
    }

    /**
     * Test of Do method, of class ControlFunctionIf.
     *
     */
    @Test
    public void bTestDo() {
        System.out.println("Do - ControlFunctionIf");
        IDecision decision = this.deciFactory.GetDecision("xy#0", "");
        ControlFunctionIf instance = new ControlFunctionIf("", decision, this.controlFunction);
        boolean result = instance.Init();
        instance.Do();
        this.values.put("xy#0", 0.0);
        instance.Do();
        assertTrue(this.values.get("xz#0").equals(3.0));
    }
}
