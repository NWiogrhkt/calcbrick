/*
 * The MIT License
 *
 * Copyright 2019 starkmrk.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package ControlFunction;

import Decision.DecisionFactory;
import Calculation.CalculationFactory;
import Decision.IDecision;
import java.util.HashMap;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author starkmrk
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ControlFunctionDeclarationTest {

    private final Map<String, Double> values;
    private final String formular;
    private final String parameter;

    public ControlFunctionDeclarationTest() {
        this.values = new HashMap<>();
        parameter = "xz#0";
        formular = String.format("var {};", parameter);
    }

    @Before
    public void setUp() {
    }

    /**
     * Test of Init method, of class ControlFunctionDeclaration.
     */
    @Test
    public void aTestInit() {
        System.out.println("Init - ControlFunctionDeclaration");
        ControlFunctionDeclaration instance = new ControlFunctionDeclaration(this.values, parameter);
        boolean result = instance.Init();
        assertTrue(result
                && this.values.containsKey(parameter)
                && this.values.get(parameter).equals(Double.NaN));
    }

    /**
     * Test of Do method, of class ControlFunctionDeclaration.
     */
    @Test
    public void bTestDo() {
        System.out.println("Do - ControlFunctionDeclaration");
        ControlFunctionDeclaration instance = new ControlFunctionDeclaration(this.values, parameter);
        boolean result = instance.Init();
        this.values.put(parameter, 1.0);
        instance.Do();
        assertTrue(result
                && this.values.containsKey(parameter)
                && this.values.get(parameter).equals(1.0));
    }

}
