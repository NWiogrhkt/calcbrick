/*
 * The MIT License
 *
 * Copyright 2019 starkmrk.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package ControlFunction;

import Decision.DecisionFactory;
import Calculation.CalculationFactory;
import Extended.NotificationMap;
import java.util.HashMap;
import java.util.Map;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author starkmrk
 */
public class ControlFunctionFactoryTest {

    private final ControlFunctionFactory instance;
    private final DecisionFactory decifactory;
    private final CalculationFactory calcFactory;
    private final NotificationMap values;

    public ControlFunctionFactoryTest() {
        Map<String, Double> valuesInside = new HashMap<>();
        this.values= new NotificationMap(valuesInside);
        this.calcFactory = new CalculationFactory(this.values);
        this.decifactory = new DecisionFactory(this.values, this.calcFactory);
        this.instance = new ControlFunctionFactory(this.values, this.calcFactory, this.decifactory);
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @Before
    public void setUp() {
        this.values.put("xy#0", 0.0);
        this.values.put("xz#0", 1.0);
        this.calcFactory.Init();
        this.decifactory.Init();
    }

    /**
     * Test of Init method, of class ControlFunctionFactory.
     */
    @Test
    public void testInit() {
        System.out.println("Init - ControlFunctionFactory");
        try {
            boolean result = instance.Init();
            assertNotEquals(null, result);
        } catch (Exception e) {
            fail("Exception while Init");
        }
    }

    /**
     * Test of GetControlFunction method, of class ControlFunctionFactory.
     */
    @Test
    public void testGetControlFunction() {
        System.out.println("GetControlFunction - ControlFunctionFactory");
        try {
            instance.Init();
            String formular = "xy#0 = true;";
            IControlFunction result = instance.GetControlFunction(formular, "");
            assertNotEquals(null, result);
        } catch (Exception e) {
            fail("Exception while GetDecision");
        }
    }

}
