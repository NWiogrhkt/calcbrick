/*
 * The MIT License
 *
 * Copyright 2019 starkmrk.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package ControlFunction;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author starkmrk
 */
public class ControlFunctionCommendTest {
    
    public ControlFunctionCommendTest() {
    }
    
    /**
     * Test of Init method, of class ControlFunctionCommend.
     */
    @Test
    public void testInit() {
        System.out.println("Init - ControlFunctionCommend");
        ControlFunctionCommend instance = new ControlFunctionCommend("");
        assertTrue(instance.Init());
    }

    /**
     * Test of Do method, of class ControlFunctionCommend.
     */
    @Test
    public void testDo() {
        System.out.println("Do - ControlFunctionCommend");
        ControlFunctionCommend instance = new ControlFunctionCommend("");
        try{
        instance.Do();
        }catch(Exception e)
        {
            fail("Exception while \"Do - Command\"");
        }
    }
    
}
