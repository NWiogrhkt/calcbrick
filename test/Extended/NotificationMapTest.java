/*
 * The MIT License
 *
 * Copyright 2019 starkmrk.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package Extended;

import java.util.HashMap;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author starkmrk
 */
public class NotificationMapTest implements IValueChangedListener {

    private final NotificationMap<String, Double> instance;
    private boolean eventFired;

    public NotificationMapTest() {
        Map<String, Double> valuesInside = new HashMap<>();
        this.instance = new NotificationMap(valuesInside);
        this.eventFired = false;
    }

    @Before
    public void setUp() {
    }

    /**
     * Test of AddListener method, of class NotificationMap.
     *
     * @Test
     */
    public void aTestAddListener() {
        System.out.println("AddListener");

        try {
            this.instance.AddListener(this);
        } catch (Exception e) {
            fail("Listener not added");
        }
    }

    /**
     * Test of put method, of class NotificationMap.
     */
    @Test
    public void bTestPut() {
        System.out.println("NotificationMap - put");
        this.instance.AddListener(this);
        boolean expResult = true;
        instance.put("xz#0", 5.5);
        boolean result = this.eventFired && this.instance.get("xz#0").equals(5.5);
        assertEquals(expResult, result);
    }

    @Override
    public void ValueChanged(Object key, Object value) {
        System.out.println("Value changed event fired");
        this.eventFired = true;
    }

}
