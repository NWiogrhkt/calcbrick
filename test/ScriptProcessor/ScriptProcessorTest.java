/*
 * The MIT License
 *
 * Copyright 2019 starkmrk.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package ScriptProcessor;

import Extended.IValueChangedListener;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author starkmrk
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ScriptProcessorTest implements IValueChangedListener {

    private boolean valueChanged;
    String[] errFiles;
    String[] files;

    public ScriptProcessorTest() {
        this.valueChanged = false;
        this.errFiles = new String[]{"test/ScriptProcessor/Failure.txt"};
        this.files = new String[]{"test/ScriptProcessor/Script1.txt", "test/ScriptProcessor/Script2.txt"};
    }

    @Before
    public void setUp() {
    }

    /**
     * Test of Init method, of class ScriptProcessor.
     */
    @Test
    public void aTestInit() {
        System.out.println("Init - ScriptProcessor");
        ScriptProcessor instance = new ScriptProcessor();
        boolean result = false;
        if (result = !instance.Init(this.errFiles)) {
            result = instance.Init(this.files);
        }
        assertTrue(result);
    }

    /**
     * Test of AddListener method, of class ScriptProcessor.
     */
    @Test
    public void bTestAddListener() {
        System.out.println("AddListener - ScriptProcessor");
        try {
            ScriptProcessor instance = new ScriptProcessor();
            instance.Init(files);
            instance.AddListener(this);
        } catch (Exception e) {
            fail("Exception @AddListener - ScriptProcessor.");
        }
    }

    /**
     * Test of ChangeValue method, of class ScriptProcessor.
     */
    @Test
    public void cTestChangeValue() {
        System.out.println("ChangeValue - ScriptProcessor");
        ScriptProcessor instance = new ScriptProcessor();
        instance.Init(files);
        boolean result = !instance.ChangeValue("", 0.0);
        result = result && instance.ChangeValue("Val1", 0.0);
        assertTrue(result);
    }

    /**
     * Test of ValueChanged method, of class ScriptProcessor.
     */
    @Test
    public void dTestValueChanged() {
        System.out.println("ValueChanged - ScriptProcessor");
        ScriptProcessor instance = new ScriptProcessor();
        instance.Init(files);
        instance.AddListener(this);
        long startTime = System.currentTimeMillis();
        instance.ChangeValue("Val1", 0.0);
        long estimatedTime = System.currentTimeMillis() - startTime;
        System.out.println("ScriptProcessor runs --> " + estimatedTime + "ms to perform the test scripts 1000 times");
        assertTrue(this.valueChanged);
    }

    public void ValueChanged(Object key, Object value) {
        //System.out.println(String.format("ValueChanged - ScriptProcessor: %s = %s", key, value.toString()));
        this.valueChanged = true;
    }
}
