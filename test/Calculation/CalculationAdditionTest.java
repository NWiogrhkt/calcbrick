/*
 * The MIT License
 *
 * Copyright 2019 starkmrk.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package Calculation;

import java.util.Map;
import java.util.HashMap;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author starkmrk
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CalculationAdditionTest {
    
    private final Map<String, Double> values;
    private CalculationAddition instance;

    public CalculationAdditionTest() {
        this.values = new HashMap<>();
    }
    
    @Before
    public void setUp() {
        this.values.put("xy#0", 12.0);
        this.values.put("xz#0", 12.0);
        CalculationReadVar c1 = new CalculationReadVar(this.values, "xy#0");
        CalculationReadVar c2 = new CalculationReadVar(this.values, "xz#0");
        instance = new CalculationAddition(c1, c2);
    }

    /**
     * Test of Init method, of class CalculationPlus.
     */
    @Test
    public void testInitCalculation() {
        System.out.println("InitCalculation - Addition");
        boolean expResult = true;
        boolean result = instance.Init();
        assertEquals(expResult, result);
    }

    /**
     * Test of Do method, of class CalculationPlus.
     */
    @Test
    public void testDoCalculation() {
        System.out.println("DoCalculation - Addition");
        Double expResult = 24.0;
        Double result = instance.Do();
        assertEquals(expResult, result);
    }

    /**
     * Test of GetLog method, of class CalculationPlus.
     */
    @Test
    public void testGetVaribaleNames() {
        System.out.println("GetVaribaleNames - Addition");
        String expResult = "12.000000 (xy#0) + 12.000000 (xz#0)";
        instance.Do();
        String result = instance.GetLog();
        assertEquals(expResult, result);
    }
    
}
