/*
 * The MIT License
 *
 * Copyright 2019 starkmrk.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package Calculation;

import java.util.Map;
import java.util.HashMap;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author starkmrk
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CalculationATanTest {
    
    private final Map<String, Double> values;
    private CalculationArcTan instance;
    
    public CalculationATanTest() {
       this.values = new HashMap<>();
    }
    
    @Before
    public void setUp() {
        this.values.put("xz#0", 1.0);
        CalculationReadVar c1 = new CalculationReadVar(this.values, "xz#0");
        instance = new CalculationArcTan(c1);
    }

    /**
     * Test of Init method, of class CalculationCos.
     */
    @Test
    public void aTestInitCalculation() {
        System.out.println("InitCalculation - ATan");
        boolean expResult = true;
        boolean result = instance.Init();
        assertEquals(expResult, result);
    }

    /**
     * Test of Do method, of class CalculationCos.
     */
    @Test
    public void bTestDoCalculation() {
        System.out.println("DoCalculation - ATan");
        Double expResult = 45.0;
        Double result = instance.Do();
        assertEquals(expResult, result);
    }

    /**
     * Test of GetLog method, of class CalculationCos.
     */
    @Test
    public void cTestGetVaribaleNames() {
        System.out.println("GetVaribaleNames - ATan");
        String expResult = "ATAN(1.000000 (xz#0))";
        instance.Do();
        String result = instance.GetLog();
        assertEquals(expResult, result);
    }
    
}
