/*
 * The MIT License
 *
 * Copyright 2019 starkmrk.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package Calculation;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author starkmrk
 */
public class ICalculationTest {
    
    public ICalculationTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @Before
    public void setUp() {
    }

    /**
     * Test of Do method, of class ICalculation.
     */
    @Test
    public void testDoCalculation() {
        System.out.println("DoCalculation - ICalculation");
        ICalculation instance = new ICalculationImpl();
        Double expResult = Double.NaN;
        Double result = instance.Do();
        assertEquals(expResult, result);
    }

    /**
     * Test of Init method, of class ICalculation.
     */
    @Test
    public void testInitCalculation() {
        System.out.println("InitCalculation - ICalculation");
        ICalculation instance = new ICalculationImpl();
        boolean expResult = false;
        boolean result = instance.Init();
        assertEquals(expResult, result);
    }

    /**
     * Test of GetVaribaleName method, of class ICalculation.
     */
    @Test
    public void testGetVaribaleNames() {
        System.out.println("GetVaribaleName - ICalculation");
        ICalculation instance = new ICalculationImpl();
        String result = instance.GetLog();
        Assert.assertEquals("", result);
     }


    public class ICalculationImpl implements ICalculation {

        public Double Do() {
            return Double.NaN;
        }

        public boolean Init() {
            return false;
        }

        public String GetLog() {
            return "";
        }
    }
    
}
