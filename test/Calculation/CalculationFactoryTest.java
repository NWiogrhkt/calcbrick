/*
 * The MIT License
 *
 * Copyright 2019 starkmrk.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package Calculation;

import Extended.NotificationMap;
import java.util.Map;
import java.util.HashMap;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author starkmrk
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CalculationFactoryTest {

    private final NotificationMap values;

    public CalculationFactoryTest() {
        Map<String, Double> valuesInside = new HashMap<>();
        this.values = new NotificationMap(valuesInside);
    }

    @Before
    public void setUp() {
        this.values.put("xy#0", 1.0);
    }

    /**
     * Test of Init method, of class CalculationFactory.
     */
    @Test
    public void testInit() {
        System.out.println("Init - CalculationFactory");
        try {
            CalculationFactory instance = new CalculationFactory(values);
            boolean result = instance.Init();
            assertEquals(true, result);
        } catch (Exception e) {
            fail("Excaption while CalculationFactory.Init");
        }
    }

    /**
     * Test of GetCalculation method, of class CalculationFactory.
     */
    @Test
    public void aTestGetCalculation() {
        System.out.println("GetCalculation - CalculationFactory");
        CalculationFactory instance = new CalculationFactory(values);
        instance.Init();
        ICalculation result;

        result = instance.GetCalculation("2", "");
        if (!(result instanceof CalculationReadConstant)) {
            fail("Failure while GetCalculation --> CalculationReadConstant");
        }
        result = instance.GetCalculation("xy#0", "");
        if (!(result instanceof CalculationReadVar)) {
            fail("Failure while GetCalculation --> CalculationReadVar");
        }
        result = instance.GetCalculation("xy#0=2", "");
        if (!(result instanceof CalculationAssign)) {
            fail("Failure while GetCalculation --> CalculationAssign");
        }
        result = instance.GetCalculation("xy#0+2", "");
        if (!(result instanceof CalculationAddition)) {
            fail("Failure while GetCalculation --> CalculationAddition");
        }
        result = instance.GetCalculation("xy#0-2", "");
        if (!(result instanceof CalculationSubtraction)) {
            fail("Failure while GetCalculation --> CalculationSubtraction");
        }
        result = instance.GetCalculation("xy#0*2", "");
        if (!(result instanceof CalculationMultiplication)) {
            fail("Failure while GetCalculation --> CalculationMultiplication");
        }
        result = instance.GetCalculation("xy#0/2", "");
        if (!(result instanceof CalculationDivision)) {
            fail("Failure while GetCalculation --> CalculationDivision");
        }
        result = instance.GetCalculation("(2)", "");
        if (!(result instanceof CalculationParentheses)) {
            fail("Failure while GetCalculation --> CalculationParentheses");
        }
        result = instance.GetCalculation("sin(2)", "");
        if (!(result instanceof CalculationSin)) {
            fail("Failure while GetCalculation --> CalculationSin");
        }
        result = instance.GetCalculation("cos(2)", "");
        if (!(result instanceof CalculationCos)) {
            fail("Failure while GetCalculation --> CalculationCos");
        }
        result = instance.GetCalculation("tan(2)", "");
        if (!(result instanceof CalculationTan)) {
            fail("Failure while GetCalculation --> CalculationTan");
        }
        result = instance.GetCalculation("asin(2)", "");
        if (!(result instanceof CalculationArcSin)) {
            fail("Failure while GetCalculation --> CalculationArcSin");
        }
        result = instance.GetCalculation("acos(2)", "");
        if (!(result instanceof CalculationArcCos)) {
            fail("Failure while GetCalculation --> CalculationArcCos");
        }
        result = instance.GetCalculation("atan(2)", "");
        if (!(result instanceof CalculationArcTan)) {
            fail("Failure while GetCalculation --> CalculationArcTan");
        }
        result = instance.GetCalculation("pow(xy#0,2)", "");
        if (!(result instanceof CalculationPow)) {
            fail("Failure while GetCalculation --> CalculationPow");
        }
        result = instance.GetCalculation("round(xy#0,2)", "");
        if (!(result instanceof CalculationRound)) {
            fail("Failure while GetCalculation --> CalculationRound");
        }

        try {
            String formular = "xy#0=5-(3+4)*SIN(30)/COS(60)+POW(2,3)";
            result = instance.GetCalculation(formular, "");
            assertNotEquals(null, result);
        } catch (Exception e) {
            fail("Exception while GetCalculation");
        }

    }
}
