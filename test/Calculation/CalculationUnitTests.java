/*
 * The MIT License
 *
 * Copyright 2019 starkmrk.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package Calculation;

import Extended.NotificationMap;
import java.util.Map;
import java.util.HashMap;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author starkmrk
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CalculationUnitTests {

    private final NotificationMap values;

    public CalculationUnitTests() {
        Map<String, Double> valuesInside = new HashMap<>();
        this.values = new NotificationMap(valuesInside);
    }

    @Before
    public void setUp() {
        this.values.put("xy#0", 1.0);
    }

    @Test
    public void bTestDoCalculation() {
        System.out.println("DoCalculation speedtest - CalculationIntegrationtests");
        int runs = 1000000;
        Double result = 0.0;
        try {
            CalculationFactory instance = new CalculationFactory(values);
            instance.Init();
            ICalculation calculation = instance.GetCalculation("xy#0=round(5-((3+4)*SIN(30))/(COS(60)+POW(2,3)),3.12)", "");
            calculation.Init();
            long startTime = System.currentTimeMillis();
            for (int i = 0; i < runs; i++) {
                result = calculation.Do();
            }
            long estimatedTime = System.currentTimeMillis() - startTime;
            System.out.println("DoCalculation: " + runs + " runs --> " + estimatedTime + "ms");
            assertTrue(4.588d == result);
        } catch (Exception e) {
            fail("Exception while GetCalculation");
        }
    }
}
