/*
 * The MIT License
 *
 * Copyright 2019 starkmrk.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package ScriptProcessor;

import Extended.*;
import ControlFunction.ControlFunctionFactory;
import ControlFunction.IControlFunctionFactory;
import ControlFunction.IControlFunction;
import Decision.DecisionFactory;
import Decision.IDecisionFactory;
import Calculation.CalculationFactory;
import Calculation.ICalculationFactory;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author starkmrk
 */
public class ScriptProcessor implements IScriptProcessor, IValueChangedListener {

    private static final Logger log = LogManager.getLogger(ScriptProcessor.class.getName());

    private final List<IValueChangedListener> listeners = new ArrayList<>();

    private final IControlFunctionFactory controlfunctionFactory;
    private final IDecisionFactory decifactory;
    private final ICalculationFactory calcFactory;
    private final Map<String, Double> valuesInside;
    private final NotificationMap<String, Double> values;
    private final Object lock;
    private String formular;
    private IControlFunction controlFunctions;

    public ScriptProcessor() {
        this.valuesInside = new HashMap<>();
        this.values = new NotificationMap(this.valuesInside);
        this.lock = new Object();
        this.calcFactory = new CalculationFactory(this.values);
        this.decifactory = new DecisionFactory(this.values, this.calcFactory);
        this.controlfunctionFactory = new ControlFunctionFactory(this.values, this.calcFactory, this.decifactory);
        this.formular = "";
    }

    @Override
    public void AddListener(IValueChangedListener toAdd) {
        this.listeners.add(toAdd);
    }

    @Override
    public boolean ChangeValue(String variable, Double value) {
        if (this.valuesInside.containsKey(variable)) {
            this.valuesInside.put(variable, value);
            this.Do();
            return true;
        }
        log.debug(String.format("'%s' not declared in scripts.", variable));
        return false;
    }

    @Override
    public boolean Init(String[] files) {
        boolean r = this.GetFormularsFromFiles(files);
        this.controlFunctions = this.controlfunctionFactory.GetControlFunction(this.formular, "");
        if (this.controlFunctions == null) {
            log.fatal("Can not parse formulars from text.");
            return false;
        }
        r = this.controlFunctions.Init() ? r : false;
        if (r) {
            this.values.AddListener(this);
        //} else {
        //    log.error(this.controlFunctions.GetLog());
        }
        return r;
    }

    @Override
    public void ValueChanged(Object key, Object value) {
        listeners.forEach((l) -> {
            l.ValueChanged(key, value);
        });
        //log.debug(String.format("Set %s = %f", key, value));
    }

    private void Do() {
        Map<String, Double> valuesCopy = new HashMap<>();
        do {
            synchronized (lock) {
                valuesCopy.putAll(valuesInside);
                this.controlFunctions.Do();
            }
            log.debug(System.getProperty("line.separator")
                    + this.controlFunctions.GetLog());
        } while (!this.valuesInside.equals(valuesCopy));
    }

    private boolean GetFormularsFromFiles(String[] files) {
        boolean r = true;
        this.formular = "";
        LinkOption[] options = new LinkOption[]{LinkOption.NOFOLLOW_LINKS};
        for (String file : files) {
            try {
                Path p = Paths.get(file).toAbsolutePath();
                if (Files.exists(p, options)) {
                    String f = new String(Files.readAllBytes(p));
                    this.formular = this.formular + f + " ";
                } else {
                    log.error(String.format("'%s' not exists!", p.toString()));
                    return false;
                }
            } catch (IOException e) {
                r = false;
                log.error(String.format("File not found: %s", file), e);
                return false;
            }
        }

        return true;
    }
}
