/*
 * The MIT License
 *
 * Copyright 2019 starkmrk.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package Decision;

import Extended.NotificationMap;
import Calculation.ICalculationFactory;
import Calculation.ICalculation;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author starkmrk
 */
public class DecisionFactory implements IDecisionFactory {

    private static final Logger log = LogManager.getLogger(DecisionFactory.class.getName());

    private final ICalculationFactory calculationFactory;
    private final NotificationMap values;
    private final Pattern isDecisionPattern;

    public DecisionFactory(NotificationMap values, ICalculationFactory calculationFactory) {
        this.values = values;
        this.calculationFactory = calculationFactory;
        String pattern = DecisionOr.RegExPattern
                + "|" + DecisionAnd.RegExPattern
                + "|" + DecisionParentheses.RegExPattern
                + "|" + DecisionBiggerOrEqual.RegExPattern
                + "|" + DecisionBigger.RegExPattern
                + "|" + DecisionSmallerOrEqual.RegExPattern
                + "|" + DecisionSmaller.RegExPattern
                + "|" + DecisionEqual.RegExPattern
                + "|" + DecisionNotEqual.RegExPattern;
        isDecisionPattern = Pattern.compile(pattern);
    }

    public boolean Init() {
        return true;
    }

    @Override
    public boolean IsDecision(String formular) {
        return this.isDecisionPattern.matcher(formular).find();
    }

    @Override
    public IDecision GetDecision(String formular, String parsedFormular) {
        Matcher m;
        IDecisionAssign a;
        IDecision d1;
        IDecision d2;
        ICalculation c1;
        ICalculation c2;
        formular = formular.trim();

        m = this.GetMatch(DecisionAssign.RegExPattern, formular);
        if (m.lookingAt()) {
            DecisionReadVar decisionReadVar = new DecisionReadVar(this.values, formular.substring(0, m.end(1)));
            a = decisionReadVar;
            d1 = decisionReadVar;
            d2 = this.GetDecision(formular.substring(m.end(2)), parsedFormular + formular.substring(m.end(2)));
            return new DecisionAssign(a, d1, d2);
        }

        m = this.GetMatch(DecisionOr.RegExPattern, formular);
        if (m.find()) {
            d1 = this.GetDecision(formular.substring(0, m.start(1)), parsedFormular);
            d2 = this.GetDecision(formular.substring(m.end(1)), parsedFormular + formular.substring(0, m.end(1)));
            return new DecisionOr(d1, d2);
        }

        m = this.GetMatch(DecisionAnd.RegExPattern, formular);
        if (m.find()) {
            d1 = this.GetDecision(formular.substring(0, m.start(1)), parsedFormular);
            d2 = this.GetDecision(formular.substring(m.end(1)), parsedFormular + formular.substring(0, m.end(1)));
            return new DecisionAnd(d1, d2);
        }

        m = this.GetMatch(DecisionParentheses.RegExPattern, formular);
        if (m.find()) {
            d1 = GetDecision(formular.substring(m.start(1), m.end(1)), parsedFormular + formular.substring(m.end(1)));
            return new DecisionParentheses(d1);
        }

        m = this.GetMatch(DecisionBiggerOrEqual.RegExPattern, formular);
        if (m.find()) {
            c1 = this.calculationFactory.GetCalculation(formular.substring(0, m.start(1)), parsedFormular);
            c2 = this.calculationFactory.GetCalculation(formular.substring(m.end(1)), parsedFormular + formular.substring(0, m.end(1)));
            return new DecisionBiggerOrEqual(c1, c2);
        }

        m = this.GetMatch(DecisionBigger.RegExPattern, formular);
        if (m.find()) {
            c1 = this.calculationFactory.GetCalculation(formular.substring(0, m.start(1)), parsedFormular);
            c2 = this.calculationFactory.GetCalculation(formular.substring(m.end(1)), parsedFormular + formular.substring(0, m.end(1)));
            return new DecisionBigger(c1, c2);
        }

        m = this.GetMatch(DecisionSmallerOrEqual.RegExPattern, formular);
        if (m.find()) {
            c1 = this.calculationFactory.GetCalculation(formular.substring(0, m.start(1)), parsedFormular);
            c2 = this.calculationFactory.GetCalculation(formular.substring(m.end(1)), parsedFormular + formular.substring(0, m.end(1)));
            return new DecisionSmallerOrEqual(c1, c2);
        }

        m = this.GetMatch(DecisionSmaller.RegExPattern, formular);
        if (m.find()) {
            c1 = this.calculationFactory.GetCalculation(formular.substring(0, m.start(1)), parsedFormular);
            c2 = this.calculationFactory.GetCalculation(formular.substring(m.end(1)), parsedFormular + formular.substring(0, m.end(1)));
            return new DecisionSmaller(c1, c2);
        }

        m = this.GetMatch(DecisionEqual.RegExPattern, formular);
        if (m.find()) {
            c1 = this.calculationFactory.GetCalculation(formular.substring(0, m.start(1)), parsedFormular);
            c2 = this.calculationFactory.GetCalculation(formular.substring(m.end(1)), parsedFormular + formular.substring(0, m.end(1)));
            return new DecisionEqual(c1, c2);
        }

        m = this.GetMatch(DecisionNotEqual.RegExPattern, formular);
        if (m.find()) {
            c1 = this.calculationFactory.GetCalculation(formular.substring(0, m.start(1)), parsedFormular);
            c2 = this.calculationFactory.GetCalculation(formular.substring(m.end(1)), parsedFormular + formular.substring(0, m.end(1)));
            return new DecisionNotEqual(c1, c2);
        }

        m = this.GetMatch(DecisionReadConstant.RegExPattern, formular);
        if (m.find() && formular.substring(m.start(1), m.end(1)).equals(formular)) {
            return new DecisionReadConstant(formular.substring(m.start(1), m.end(1)));
        }

        m = this.GetMatch(DecisionReadVar.RegExPattern, formular);
        if (m.lookingAt()) {
            return new DecisionReadVar(this.values, formular.substring(m.start(1), m.end(1)));
        }

        int pfs = parsedFormular.length() <= 31 ? 0 : parsedFormular.length() - 30;
        int pfe = parsedFormular.length();
        int fs = 0;
        int fe = formular.length() < 20 ? formular.length() : 20;
        fe = fe < 0 ? 0 : fe;
        log.error(String.format("%s --> %s",
                parsedFormular.substring(pfs, pfe), formular.substring(fs, fe)));
        return null;
    }

    private Matcher GetMatch(String pattern, String formular) {
        String s = "";
        int l = 0;
        for (char ch : formular.toCharArray()) {
            if (ch == '(') {
                s = l > 0 ? s + "$" : s + ch;
                l++;
                continue;
            }
            if (ch == ')') {
                l--;
                s = l > 0 ? s + "$" : s + ch;
                continue;
            }
            if (l < 0) {
                throw new java.lang.Error("Parentheses!");
            }
            if (l == 0 || ch == ',') {
                s = s + ch;
                continue;
            }
            if (l > 0) {
                s = s + "$";
                continue;
            }
            throw new java.lang.Error("Parentheses!");
        }

        return Pattern.compile(pattern).matcher(s);
    }
}
