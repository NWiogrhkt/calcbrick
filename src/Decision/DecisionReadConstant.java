/*
 * The MIT License
 *
 * Copyright 2019 starkmrk.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package Decision;

/**
 *
 * @author starkmrk
 */
public class DecisionReadConstant implements IDecision {

    public static String RegExPattern = "^\\s*((?:[Tt][Rr][Uu][Ee])|(?:[Ff][Aa][Ll][Ss][Ee]))\\s*";
    private final String constant;
    private boolean value;

    public DecisionReadConstant(String constant) {
        this.constant = constant;
    }

    @Override
    public boolean Init() {
        try {
            this.value = Boolean.valueOf(this.constant);
            return true;
        } catch (NumberFormatException | NullPointerException e) {
            this.value = false;
            return false;
        }
    }

    @Override
    public boolean Do() {
        return this.value;
    }

    @Override
    public boolean GetDecisionStatus() {
        return this.value;
    }

    @Override
    public String GetLog() {
        return this.value ? "true" : "false";
    }
}
