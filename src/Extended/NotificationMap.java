package Extended;

/*
 * The MIT License
 *
 * Copyright 2019 starkmrk.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author starkmrk
 */
public class NotificationMap<K, V> implements Map<K, V> {

    private final Map<K, V> delegatee;
    private final List<IValueChangedListener> listeners = new ArrayList<>();

    public NotificationMap(Map<K, V> delegatee) {
        this.delegatee = delegatee;
    }

    public void AddListener(IValueChangedListener toAdd) {
        this.listeners.add(toAdd);
    }

    @Override
    public int size() {
        return this.delegatee.size();
    }

    @Override
    public boolean isEmpty() {
        return this.delegatee.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        return this.delegatee.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return this.delegatee.containsValue(value);
    }

    @Override
    public V get(Object key) {
        return this.delegatee.get(key);
    }

    @Override
    public V put(K key, V value) {
        if (!this.containsKey(key) || !this.delegatee.get(key).equals(value)) {
            this.delegatee.put(key, value);
            listeners.forEach((l) -> {
                l.ValueChanged(key, value);
            });
        }
        return value;
    }

    @Override
    public V remove(Object key) {
        return this.delegatee.remove(key);
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
        this.delegatee.putAll(m);
    }

    @Override
    public void clear() {
        this.delegatee.clear();
    }

    @Override
    public Set<K> keySet() {
        return this.delegatee.keySet();
    }

    @Override
    public Collection<V> values() {
        return this.delegatee.values();
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        return this.delegatee.entrySet();
    }

}
