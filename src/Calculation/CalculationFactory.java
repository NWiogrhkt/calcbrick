/*
 * The MIT License
 *
 * Copyright 2019 starkmrk.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package Calculation;

import Extended.NotificationMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author starkmrk
 */
public class CalculationFactory implements ICalculationFactory {

    private static final Logger log = LogManager.getLogger(CalculationFactory.class.getName());

    private final NotificationMap values;

    public CalculationFactory(NotificationMap values) {
        this.values = values;
    }

    public boolean Init() {
        return true;
    }

    @Override
    public ICalculation GetCalculation(String formular, String parsedFormular) {

        Matcher m;
        ICalculationAssign a;
        ICalculation t1;
        ICalculation t2;

        m = this.GetMatch(CalculationAssign.RegExPattern, formular);
        if (m.lookingAt()) {
            CalculationReadVar calculationReadVar = new CalculationReadVar(this.values, formular.substring(0, m.end(1)));
            a = calculationReadVar;
            t1 = calculationReadVar;
            t2 = this.GetCalculation(formular.substring(m.end(2)), parsedFormular + formular.substring(0, m.end(1)));
            return new CalculationAssign(a, t1, t2);
        }

        m = this.GetMatch(CalculationAddition.RegExPattern, formular);
        if (m.find()) {
            t1 = this.GetCalculation(formular.substring(0, m.start(1)), parsedFormular);
            t2 = this.GetCalculation(formular.substring(m.end(1)), parsedFormular + formular.substring(0, m.end(1)));
            return new CalculationAddition(t1, t2);
        }

        m = this.GetMatch(CalculationSubtraction.RegExPattern, formular);
        if (m.find()) {
            t1 = this.GetCalculation(formular.substring(0, m.start(1)), parsedFormular);
            t2 = this.GetCalculation(formular.substring(m.end(1)), parsedFormular + formular.substring(0, m.end(1)));
            return new CalculationSubtraction(t1, t2);
        }

        m = this.GetMatch(CalculationMultiplication.RegExPattern, formular);

        if (m.find()) {
            t1 = this.GetCalculation(formular.substring(0, m.start(1)), parsedFormular);
            t2 = this.GetCalculation(formular.substring(m.end(1)), parsedFormular + formular.substring(0, m.end(1)));
            return new CalculationMultiplication(t1, t2);
        }

        m = this.GetMatch(CalculationDivision.RegExPattern, formular);
        if (m.find()) {
            t1 = this.GetCalculation(formular.substring(0, m.start(1)), parsedFormular);
            t2 = this.GetCalculation(formular.substring(m.end(1)), parsedFormular + formular.substring(0, m.end(1)));
            return new CalculationDivision(t1, t2);
        }

        m = this.GetMatch(CalculationParentheses.RegExPattern, formular);
        if (m.find()) {
            t1 = GetCalculation(formular.substring(m.start(1), m.end(1)), parsedFormular + formular.substring(m.start(1)));
            return new CalculationParentheses(t1);
        }

        m = this.GetMatch(CalculationSin.RegExPattern, formular);
        if (m.find()) {
            t1 = GetCalculation(formular.substring(m.start(1), m.end(1)), parsedFormular + formular.substring(m.start(1)));
            return new CalculationSin(t1);
        }

        m = this.GetMatch(CalculationCos.RegExPattern, formular);
        if (m.find()) {
            t1 = GetCalculation(formular.substring(m.start(1), m.end(1)), parsedFormular + formular.substring(m.start(1)));
            return new CalculationCos(t1);
        }

        m = this.GetMatch(CalculationTan.RegExPattern, formular);
        if (m.find()) {
            t1 = GetCalculation(formular.substring(m.start(1), m.end(1)), parsedFormular + formular.substring(m.start(1)));
            return new CalculationTan(t1);
        }

        m = this.GetMatch(CalculationArcSin.RegExPattern, formular);
        if (m.find()) {
            t1 = GetCalculation(formular.substring(m.start(1), m.end(1)), parsedFormular + formular.substring(m.start(1)));
            return new CalculationArcSin(t1);
        }

        m = this.GetMatch(CalculationArcCos.RegExPattern, formular);
        if (m.find()) {
            t1 = GetCalculation(formular.substring(m.start(1), m.end(1)), parsedFormular + formular.substring(m.start(1)));
            return new CalculationArcCos(t1);
        }

        m = this.GetMatch(CalculationArcTan.RegExPattern, formular);
        if (m.find()) {
            t1 = GetCalculation(formular.substring(m.start(1), m.end(1)), parsedFormular + formular.substring(m.start(1)));
            return new CalculationArcTan(t1);
        }

        m = this.GetMatch(CalculationPow.RegExPattern, formular);
        if (m.find()) {
            t1 = GetCalculation(formular.substring(m.start(1), m.end(1)), parsedFormular);
            t2 = GetCalculation(formular.substring(m.start(2), m.end(2)), parsedFormular + formular.substring(0, m.start(2)));
            return new CalculationPow(t1, t2);
        }

        m = this.GetMatch(CalculationRound.RegExPattern, formular);
        if (m.find()) {
            t1 = GetCalculation(formular.substring(m.start(1), m.end(1)), parsedFormular);
            t2 = GetCalculation(formular.substring(m.start(2), m.end(2)), parsedFormular + formular.substring(0, m.start(2)));
            return new CalculationRound(t1, t2);
        }

        m = this.GetMatch(CalculationReadConstant.RegExPattern, formular);
        if (m.find() && formular.substring(m.start(1), m.end(1)).equals(formular.trim())) {
            return new CalculationReadConstant(formular.substring(m.start(1), m.end(1)));
        }

        m = this.GetMatch(CalculationReadVar.RegExPattern, formular);
        if (m.lookingAt()) {
            return new CalculationReadVar(this.values, formular.substring(m.start(1), m.end(1)));
        }

        int pfs = parsedFormular.length() <= 31 ? 0 : parsedFormular.length() - 30;
        int pfe = parsedFormular.length();
        int fs = 0;
        int fe = formular.length() < 20 ? formular.length() : 20;
        fe = fe < 0 ? 0 : fe;
        log.error(String.format("%s --> %s",
                parsedFormular.substring(pfs, pfe), formular.substring(fs, fe)));
        return null;
    }

    private Matcher GetMatch(String pattern, String formular) {
        Matcher m;
        String s = "";
        int l = 0;
        for (char ch : formular.toCharArray()) {
            if (ch == '(') {
                s = l > 0 ? s + "$" : s + ch;
                l++;
                continue;
            }
            if (ch == ')') {
                l--;
                s = l > 0 ? s + "$" : s + ch;
                continue;
            }
            if (l < 0) {
                throw new java.lang.Error("Parentheses!");
            }
            if (l == 0 || ch == ',') {
                s = s + ch;
                continue;
            }
            if (l > 0) {
                s = s + "$";
                continue;
            }
            throw new java.lang.Error("Parentheses!");
        }

        return Pattern.compile(pattern).matcher(s);
    }
}
