/*
 * The MIT License
 *
 * Copyright 2019 starkmrk.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package Calculation;


import static java.lang.Math.atan;
import static java.lang.Math.toDegrees;

/**
 *
 * @author starkmrk
 */
public class CalculationArcTan implements ICalculation {

    public static String RegExPattern = "^[Aa][Tt][Aa][Nn]\\(\\s*(.+)\\s*\\)";
    private final ICalculation expression1;

    public CalculationArcTan(ICalculation expression1) {
        this.expression1 = expression1;
    }

    @Override 
    public boolean Init() {
        return this.expression1.Init();
    }

    @Override
    public Double Do() {
        Double result1;
        result1 = expression1.Do();

        if (result1 != null) {
            return toDegrees(atan(result1));
        } else {
            return Double.NaN;
        }
    }

    @Override 
    public String GetLog() {
        return String.format("ATAN(%s)", this.expression1.GetLog());
    }
}
