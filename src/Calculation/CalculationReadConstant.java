/*
 * The MIT License
 *
 * Copyright 2019 starkmrk.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package Calculation;

import java.util.Locale;

/**
 *
 * @author starkmrk
 */
public class CalculationReadConstant implements ICalculation {

    public static String RegExPattern = "^\\s*((?:\\d+\\.?\\d*)|([Tt][Rr][Uu][Ee])|[Ff][Aa][Ll][Ss][Ee]|[Nn][Aa][Nn])\\s*";
    private final String constant;
    private Double value;

    public CalculationReadConstant(String constant) {
        this.constant = constant;
    }

    @Override
    public boolean Init() {
        try {
            if (this.constant.toLowerCase().equals("true")
                    || this.constant.toLowerCase().equals("false")) {
                this.value = Boolean.valueOf(this.constant) ? 1.0 : 0.0;
            } else if (this.constant.toLowerCase().equals("nan")) {
                this.value = Double.NaN;
            } else {
                this.value = Double.valueOf(this.constant);
            }
            return true;
        } catch (NumberFormatException | NullPointerException e) {
            this.value = null;
            return false;
        }
    }

    @Override
    public Double Do() {
        return this.value;
    }

    @Override
    public String GetLog() {
        return String.format(Locale.US, "%f", this.value);
    }
}
