/*
 * The MIT License
 *
 * Copyright 2019 starkmrk.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package Calculation;

import java.util.Locale;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author starkmrk
 */
public class CalculationReadVar implements ICalculation, ICalculationAssign {

    private static final Logger log = LogManager.getLogger(CalculationReadVar.class.getName());

    public static String RegExPattern = "^\\s*([a-zA-Z][\\w#]*)\\s*";
    private final Map<String, Double> values;
    private final String variable;
    private Double logValue;

    public CalculationReadVar(Map<String, Double> values, String variable) {
        this.values = values;
        this.variable = variable;
        this.logValue = Double.NaN;
    }

    @Override
    public boolean Init() {
        if (!values.containsKey(this.variable)) {
            log.error(String.format("'%s' not declared in scripts.", this.variable));
            return false;
        }
        return true;
    }

    @Override
    public Double Do() {
        if (this.values.containsKey(this.variable)) {
            this.logValue = values.get(this.variable);
        } else {
            this.logValue = Double.NaN;
        }
        return this.logValue;
    }

    public String GetVarName() {
        return this.variable;
    }

    @Override
    public double AssignVar(Double value) {
        if (value != null && this.values.containsKey(this.variable)) {
            this.logValue = value;
            values.put(this.variable, value);
            return value;
        } else {
            this.logValue = Double.NaN;
            return Double.NaN;
        }
    }

    @Override
    public String GetLog() {
        return String.format(Locale.US, "%f (%s)", this.logValue, this.variable);
    }
}
