/*
 * The MIT License
 *
 * Copyright 2019 starkmrk.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package Calculation;

/**
 *
 * @author starkmrk
 */
public class CalculationSubtraction implements ICalculation {

    public static String RegExPattern = "\\s*(\\-)\\s*";
    private final ICalculation expression1;
    private final ICalculation expression2;

    public CalculationSubtraction(ICalculation expression1, ICalculation expression2) {
        this.expression1 = expression1;
        this.expression2 = expression2;
    }

    @Override
    public boolean Init() {
        return this.expression1.Init()
                && this.expression2.Init();
    }

    @Override
    public Double Do() {
        Double result1;
        Double result2;
        result1 = expression1.Do();
        result2 = expression2.Do();

        if (result1 != null && result2 != null) {
            return result1 - result2;
        } else {
            return Double.NaN;
        }
    }

    @Override
    public String GetLog() {
        return String.format("%s - %s", this.expression1.GetLog(),
                this.expression2.GetLog());
    }
}
