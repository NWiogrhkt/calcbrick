/*
 * The MIT License
 *
 * Copyright 2019 starkmrk.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package ControlFunction;

import Calculation.ICalculation;
import Decision.IDecision;

/**
 *
 * @author starkmrk
 */
public class ControlFunctionLine implements IControlFunction {

    public static String RegExPattern = "^\\s*(.*?)\\s*\\;\\s*";
    private final IDecision decision;
    private final ICalculation calculation;
    private boolean isDecision;

    public ControlFunctionLine(IDecision decision, ICalculation calculation) {
        this.isDecision = false;
        this.decision = decision;
        this.calculation = calculation;
    }

    @Override
    public boolean Init() {
        if (this.calculation != null) {
            return this.calculation.Init();
        }
        if (this.decision != null) {
            this.isDecision = true;
            return this.decision.Init();
        }
        return false;
    }

    @Override
    public void Do() {
        if (this.isDecision) {
            this.decision.Do();
        } else {
            this.calculation.Do();
        }
    }

    @Override
    public String GetLog() {
        String decision = this.isDecision ? this.decision.GetLog()
                : this.calculation.GetLog();
        return String.format("%s;", decision);
    }
}
