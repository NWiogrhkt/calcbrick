/*
 * The MIT License
 *
 * Copyright 2019 starkmrk.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package ControlFunction;

import java.util.Map;

/**
 *
 * @author starkmrk
 */
public class ControlFunctionDeclaration implements IControlFunction {

    public static String RegExPattern = "^\\s*[Vv][Aa][Rr]\\s*([A-Za-z][A-Za-z0-9_#]*)\\s*\\;\\s*";
    private final String variable;
    private final Map<String, Double> values;

    public ControlFunctionDeclaration(Map<String, Double> values, String variable) {
        this.values = values;
        this.variable = variable;
    }

    @Override
    public boolean Init() {
        if (values.containsKey(this.variable)) {
            return false;
        }
        values.put(this.variable, Double.NaN);
        return true;
    }

    @Override
    public void Do() {
        // values.put(this.variable, Double.NaN);
    }
    
    @Override
    public String GetLog() {
        return String.format("Var %s;", this.variable);
    }

}
