/*
 * The MIT License
 *
 * Copyright 2019 starkmrk.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package ControlFunction;

import Decision.IDecision;
import java.util.List;

/**
 *
 * @author starkmrk
 */
public class ControlFunctionElse implements IControlFunction {

    public static String RegExPattern = "\\s*[Ee][Ll][Ss][Ee]\\s*\\{((?:.|\\n|\\r)*)\\s*\\}\\s*";
    private final List<IDecision> decisions;
    private final IControlFunction controlFunction;
    private String indent;

    public ControlFunctionElse(String indent, List<IDecision> decisions, IControlFunction controlFunction) {
        this.indent = indent;
        this.decisions = decisions;
        this.controlFunction = controlFunction;
    }

    @Override
    public boolean Init() {
        boolean b;
        return this.decisions.stream().allMatch(decision -> decision.Init())
                && controlFunction.Init();
    }

    @Override
    public void Do() {
        if (!this.decisions.stream().anyMatch(decision -> decision.GetDecisionStatus() == true)) {
            controlFunction.Do();
        }
    }

    @Override
    public String GetLog() {
        return String.format("ELSE{%s%s%s}", System.getProperty("line.separator"),
                this.controlFunction.GetLog(),
                indent);
    }
}
