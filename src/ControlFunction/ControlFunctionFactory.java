/*
 * The MIT License
 *
 * Copyright 2019 starkmrk.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package ControlFunction;

import Calculation.ICalculation;
import Calculation.ICalculationFactory;
import Decision.IDecision;
import Decision.IDecisionFactory;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author starkmrk
 */
public class ControlFunctionFactory implements IControlFunctionFactory {

    private static final Logger log = LogManager.getLogger(ControlFunctionFactory.class.getName());

    private final ICalculationFactory calculationFactory;
    private final IDecisionFactory decisionFactory;
    private final Map<String, Double> values;
    private int level;

    public ControlFunctionFactory(Map<String, Double> values, ICalculationFactory calculationFactory, IDecisionFactory decisionFactory) {
        this.values = values;
        this.calculationFactory = calculationFactory;
        this.decisionFactory = decisionFactory;
        this.level = -1;
    }

    public boolean Init() {
        return true;
    }

    @Override
    public IControlFunction GetControlFunction(String formular, String parsedFormular) {
        Matcher m = null;
        List<IDecision> ifDecisions = new ArrayList<>();
        ifDecisions.clear();
        this.level++;
        String indent = "";
        for (int i = 0; i < level; i++) {
            indent = indent + "  ";
        }
        ControlFunctionLevel thisLevel = new ControlFunctionLevel(indent);

        while (true) {

            if (m != null) {
                parsedFormular = parsedFormular + formular.substring(m.start(0), m.end(0));
                formular = formular.substring(m.end(0)).trim();
            }
            if (formular.trim().isEmpty()) {
                break;
            }

            m = this.GetMatch(ControlFunctionDeclaration.RegExPattern, formular);
            if (m.lookingAt()) {
                IControlFunction controlFunction = new ControlFunctionDeclaration(this.values, formular.substring(m.start(1), m.end(1)));
                thisLevel.AddControlFunction(controlFunction);
                continue;
            }

            m = this.GetMatch(ControlFunctionIf.RegExPattern, formular);
            if (m.lookingAt()) {
                ifDecisions.clear();
                IDecision decision = this.decisionFactory.GetDecision(formular.substring(m.start(1), m.end(1)), parsedFormular + "If(");
                ifDecisions.add(decision);
                parsedFormular = parsedFormular + ")" + System.lineSeparator() + indent + "{" + System.lineSeparator();
                IControlFunction controlFunctionLevel = this.GetControlFunction(formular.substring(m.start(2), m.end(2)), parsedFormular);
                IControlFunction controlFunction = new ControlFunctionIf(indent, decision, controlFunctionLevel);
                thisLevel.AddControlFunction(controlFunction);
                continue;
            }

            m = this.GetMatch(ControlFunctionElseIf.RegExPattern, formular);
            if (m.lookingAt()) {
                IDecision decision = this.decisionFactory.GetDecision(formular.substring(m.start(1), m.end(1)), parsedFormular + "ElseIf(");
                ifDecisions.add(decision);
                parsedFormular = parsedFormular + ")" + System.lineSeparator() + indent + "{" + System.lineSeparator();
                IControlFunction controlFunctionLevel = this.GetControlFunction(formular.substring(m.start(2), m.end(2)), parsedFormular);
                IControlFunction controlFunction = new ControlFunctionElseIf(indent, decision, controlFunctionLevel);
                thisLevel.AddControlFunction(controlFunction);
                continue;
            }

            m = this.GetMatch(ControlFunctionElse.RegExPattern, formular);
            if (m.lookingAt()) {
                IControlFunction controlFunctionLevel = this.GetControlFunction(formular.substring(m.start(1), m.end(1)), "else {" + parsedFormular);
                List<IDecision> elseDecisions = new ArrayList<>();
                ifDecisions.forEach(d -> elseDecisions.add(d));
                parsedFormular = parsedFormular + "Else" + System.lineSeparator() + indent + "{" + System.lineSeparator();
                IControlFunction controlFunction = new ControlFunctionElse(indent, elseDecisions, controlFunctionLevel);
                thisLevel.AddControlFunction(controlFunction);
                continue;
            }
            ifDecisions.clear();

            m = this.GetMatch(ControlFunctionLine.RegExPattern, formular);
            if (m.lookingAt()) {
                String subst = formular.substring(m.start(1), m.end(1)).trim();
                if (!subst.isEmpty()) {
                    ICalculation calculation = null;
                    IDecision decision = null;
                    if (this.decisionFactory.IsDecision(subst)) {
                        decision = this.decisionFactory.GetDecision(subst, parsedFormular);
                    } else {
                        calculation = this.calculationFactory.GetCalculation(subst, parsedFormular);
                    }
                    IControlFunction controlFunction = new ControlFunctionLine(decision, calculation);
                    thisLevel.AddControlFunction(controlFunction);
                }
                continue;
            }

            m = this.GetMatch((ControlFunctionCommend.RegExPattern), formular);
            if (m.lookingAt()) {
                String subst = formular.substring(m.start(0), m.end(0)).trim();
                IControlFunction controlFunction = new ControlFunctionCommend(subst);
                thisLevel.AddControlFunction(controlFunction);
                continue;
            }

            int pfs = parsedFormular.length() <= 31 ? 0 : parsedFormular.length() - 30;
            int pfe = parsedFormular.length();
            int fs = 0;
            int fe = formular.length() < 20 ? formular.length() : 20;
            fe = fe < 0 ? 0 : fe;
            log.error(String.format("%s --> %s",
                    parsedFormular.substring(pfs, pfe), formular.substring(fs, fe)));
            return null;

        }
        this.level--;
        return thisLevel;
    }

    private Matcher GetMatch(String pattern, String formular) {
        Matcher m;
        String s = "";
        int l = 0;
        for (char ch : formular.toCharArray()) {
            if (ch == '{') {
                s = l > 0 ? s + "$" : s + ch;
                l++;
                continue;
            }
            if (ch == '}') {
                l--;
                if (l > 0) {
                    s = s + "$";
                    continue;
                }
                s = s + ch;
                break;
            }
            if (l < 0) {
                throw new java.lang.Error("Parentheses!");
            }
            if (l == 0 || ch == ',') {
                s = s + ch;
                continue;
            }
            if (l > 0) {
                s = s + "$";
                continue;
            }
            throw new java.lang.Error("Parentheses!");
        }

        return Pattern.compile(pattern, Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE | Pattern.MULTILINE).matcher(s);
    }

}
