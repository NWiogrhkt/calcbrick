package ControlFunction;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author starkmrk
 */
public interface IControlFunctionFactory {

    IControlFunction GetControlFunction(String formular, String parsedFormular);

}
